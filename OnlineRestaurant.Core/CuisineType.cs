﻿namespace OnlineRestaurant.Core
{
    public enum CuisineType
    {
        None,
        Mexican,
        Italian,
        Indian
    }
}